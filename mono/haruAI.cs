﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace hwo2014
{
	public class haruAI : AIBase
	{
		List<double> LenSum = new List<double>();
		/// <summary>
		/// gameTickに対して、どの位置にいるか？
		/// </summary>
		List<double> nowPosList = new List<double>();
		/// <summary>
		/// そのピースで出せる最大速度
		/// </summary>
		double[] LimitSpeed;

		List<double> angleHistory = new List<double>();

		CarPosition mypos;

		public override Tuple<double, SwitchDirection?> UpdateThrottle(List<CarPosition> pos, int gameTick)
		{
			if (gameTick == 0)
			{
				//init
				LimitSpeed = new double[RaceInformation.track.pieces.Count];
				LenSum.Add(0);
				angleHistory.Add(0);
				for (int i = 0; i < RaceInformation.track.pieces.Count; i++)
				{
					var r = RaceInformation.track.pieces[i];
					if (r.radius == null)//直線の場合
					{
						LimitSpeed[i] = 100;
						LenSum.Add(LenSum[LenSum.Count - 1] + (double)r.length);
					}
					else//カーブの場合
					{
						LimitSpeed[i] = CurveSpeed((double)r.radius, (double)r.angle);
						LenSum.Add(LenSum[LenSum.Count - 1] +
							2 * (double)r.radius * Math.PI * Math.Abs((double)r.angle) / 360);
					}
				}
				return Tuple.Create<double, SwitchDirection?>(1, null);
			}
			mypos = pos.First(p => p.id.color == this.MyCarId.color);
			angleHistory.Add(Math.Abs(mypos.angle));
			var thisPieceIndex = mypos.piecePosition.pieceIndex;
			nowPosList.Add(LenSum[thisPieceIndex] + mypos.piecePosition.inPieceDistance);
			if (gameTick == 1) return Tuple.Create<double, SwitchDirection?>(1, null);
			double speed = nowPosList[nowPosList.Count - 1] - nowPosList[nowPosList.Count - 2];
			Console.WriteLine(speed);
			
			double res = SpeedControl(speed,thisPieceIndex);
			if (angleHistory[angleHistory.Count - 1] < angleHistory[angleHistory.Count - 2]) res = 1;
			
			var mod = RaceInformation.track.pieces.Count;
			if (LimitSpeed[thisPieceIndex] > LimitSpeed[(thisPieceIndex + 1) % mod])
			{
				var pp = speed - LimitSpeed[(thisPieceIndex + 1) % mod];
				if (pp > 0)
				{
					var nokori = LenSum[(thisPieceIndex + 1) % mod]
					- LenSum[thisPieceIndex] - mypos.piecePosition.inPieceDistance;
					var yoyu = SlowLength(speed, LimitSpeed[(thisPieceIndex + 1) % mod]);
					if (yoyu > nokori) res = 0;
				}
			}
			return Tuple.Create<double, SwitchDirection?>(res, null);
		}

		/// <summary>
		/// 指定する速度まで減速するのに、どのくらいの距離が必要かを返す
		/// </summary>
		/// <returns></returns>
		private double SlowLength(double nowSpeed,double targetSpeed)
		{
			//スロットルを0にすると線形に減速するモデルで考えている。
			//おそらく非線形なので要改良
			var pp = nowSpeed - targetSpeed;
			return (pp / 0.08) * (nowSpeed + targetSpeed) / 2;
		}

		/// <summary>
		/// 目的の速度に制御する
		/// </summary>
		private double SpeedControl(double nowSpeed, int thisPieceIndex)
		{
			//単純な制御
			double res;
			if (LimitSpeed[thisPieceIndex] > nowSpeed) res = 1;
			else res = 0;
			return res;
		}

		/// <summary>
		/// カーブの速度閾値を求める
		/// </summary>
		/// <param name="radius"></param>
		/// <param name="angle"></param>
		/// <returns></returns>
		private double CurveSpeed(double radius, double angle)
		{
			const double t200 = 9;
			return (t200 - 6.6) / 100 * (radius - 100) + 6.6;
		}
	}
}
