﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using v_type = System.Int32;

namespace hwo2014
{
    public struct Point2D
    {
        public readonly v_type X, Y;

        public Point2D(int x, int y)
        {
            X = (v_type)x;
            Y = (v_type)y;
        }

        public static Point2D operator +(Point2D pt, Point2D offset)
        {
            return new Point2D(pt.X + offset.X, pt.Y + offset.Y);
        }

        public static Point2D operator -(Point2D pt, Point2D offset)
        {
            return new Point2D(pt.X - offset.X, pt.Y - offset.Y);
        }

        public static Point2D operator -(Point2D pt)
        {
            return new Point2D(-pt.X, -pt.Y);
        }

        public static bool operator ==(Point2D left, Point2D right)
        {
            return ((left.X == right.X) && (left.Y == right.Y));
        }

        public static bool operator !=(Point2D left, Point2D right)
        {
            return !(left == right);
        }

        public override string ToString()
        {
            return "{X=" + this.X + ",Y=" + this.Y + "}";
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Point2D)) return false;
            Point2D ind = (Point2D)obj;
            return this.Equals(ind);
        }

        public override int GetHashCode()
        {
            return (X ^ Y);
        }
    }
}
