﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace hwo2014
{
	/// <summary>
	/// レースに出場してる車のIDを表す。
	/// </summary>
    public class CarId
    {
		/// <summary>
		/// 名前
		/// </summary>
        public string name { get; set; }
		/// <summary>
		/// 色
		/// </summary>
        public string color { get; set; }
    }
}
