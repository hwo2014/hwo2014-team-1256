using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Diagnostics;
using System.IO;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Linq;

namespace hwo2014
{
    public class Program
    {
        public static void Main(string[] args)
        {
            string host = args[0];
            int port = int.Parse(args[1]);
            string botName = args[2];
            string botKey = args[3];

            Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

            using (TcpClient client = new TcpClient(host, port))
            {
                NetworkStream stream = client.GetStream();
                StreamReader reader = new StreamReader(stream);
                StreamWriter writer = new StreamWriter(stream);
                writer.AutoFlush = true;

                AIBase ai = new haruAI();
                ai.Reader = reader;
                ai.Writer = writer;
                //var msg = new TestRaceInfo(TestRaceInfo.InfoType.join, botName, botKey);
                //msg.carCount = 1;
                //msg.trackName = "germany";
                var msg = new Join(botName, botKey);
                ai.JoinMessage = msg;
                ai.Run();
            }
        }

    }
}
