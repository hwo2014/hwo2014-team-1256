﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace hwo2014
{
    internal class TestRaceInfo : SendMsg
    {
        public enum InfoType
        {
            create,
            join
        }

        public class BotId
        {
            public string name { get; set; }
            public string key { get; set; }
        }
        public BotId botId { get; set; }
        public string trackName { get; set; }
        public string password { get; set; }
        public int carCount { get; set; }
        private InfoType type;

        public TestRaceInfo(InfoType type, string name, string key)
        {
            this.type = type;
            this.botId = new BotId();
            this.botId.name = name;
            this.botId.key = key;
        }

        protected override string MsgType()
        {
            return type.ToString() + "Race";
        }
    }
}
