﻿## C# / Mono bot for HWO 2014

Install the Mono framework and enjoy!

- [OSX](http://mono-project.com/Mono:OSX)
- [Ubuntu](http://mono-project.com/DistroPackages/Ubuntu)
- [Debian](http://mono-project.com/DistroPackages/Debian)

------

## AIの作り方

- AIBaseを継承します。
- UpdateThrottleを実装します。(SugeeAI.csを参照)
- Program.csのSugeeAIになってるところを自分のAIのクラスにします
- 楽しい！！✌('ω'✌ )三✌('ω')✌三( ✌'ω')✌
