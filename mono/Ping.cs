﻿namespace hwo2014
{
    internal class Ping : SendMsg
    {
        protected override string MsgType()
        {
            return "ping";
        }
    }
}