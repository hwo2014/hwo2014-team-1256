﻿using Newtonsoft.Json;
using System;

namespace hwo2014
{
    public abstract class SendMsg
    {
        public string ToJson()
        {
            return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
        }

        protected virtual Object MsgData()
        {
            return this;
        }

        protected abstract string MsgType();
    }
}