﻿using Newtonsoft.Json.Linq;
using System;

namespace hwo2014
{
    internal class MsgWrapper
    {
		/// <summary>
		/// メッセージのタイプ
		/// </summary>
        public string msgType
        {
            get;
            set;
        }
		/// <summary>
		/// メッセージの中身
		/// </summary>
        public JToken data
        {
            get;
            set;
        }
		/// <summary>
		/// ゲームのID
		/// </summary>
        public string gameId { get; set; }
        public int? gameTick { get; set; }

        public MsgWrapper(string msgType, object data)
        {
            this.msgType = msgType;
            if (data != null)
            {
                this.data = JToken.FromObject(data);
            }
        }
    }
}