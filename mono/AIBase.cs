﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace hwo2014
{
    public abstract class AIBase
    {
        public StreamWriter Writer
        {
            get;
            set;
        }

        public StreamReader Reader
        {
            get;
            set;
        }

        public SendMsg JoinMessage
        {
            get;
            set;
        }

        public CarId MyCarId
        {
            get;
            set;
        }

        public Race RaceInformation
        {
            get;
            set;
        }

        /// <summary>
        /// 車のIdがセットされたときに呼ばれる。
        /// </summary>
        protected virtual void UpdatedMyCarId()
        {
        }

        /// <summary>
        /// ゲームが初期化されたときに呼ばれる。
        /// このタイミングでRaceInformationが更新される。
        /// </summary>
        protected virtual void GameInitialized()
        {
        }

        /// <summary>
        /// ゲーム開始時に呼び出される。
        /// 本番はqualifying sessionがあるので2回呼ばれることに注意。
        /// </summary>
        protected virtual void GameStart()
        {
        }

        /// <summary>
        /// ゲーム終了時に呼び出される。
        /// 本番はqualifying sessionがあるので2回呼ばれることに注意。
        /// </summary>
        /// <param name="result">結果</param>
        protected virtual void GameEnded(GameResult result)
        {
        }

        /// <summary>
        /// 誰かがクラッシュした
        /// </summary>
        /// <param name="id"></param>
        protected virtual void Crashed(CarId id, int gameTick)
        {
        }

        /// <summary>
        /// クラッシュした車がトラックに戻った
        /// </summary>
        /// <param name="id"></param>
        protected virtual void Spawned(CarId id, int gameTick)
        {
        }

		/// <summary>
		/// ラップが終了した
		/// </summary>
		/// <param name="info"></param>
		/// <param name="gameTick"></param>
        protected virtual void LapFinished(LapInfo info, int gameTick)
        {
        }

        /// <summary>
        /// ここでThrottleの値を計算してください。
        /// レーンをスイッチするときは、各ピースでは最後の一回のスイッチ命令のみが有効なことに注意
        /// </summary>
        /// <param name="pos">各車の位置情報</param>
        /// <returns>Throttleの値(0-1)とレーンをスイッチするかどうか</returns>
        public abstract Tuple<double, SwitchDirection?> UpdateThrottle(List<CarPosition> pos, int gameTick);

        private Tuple<double, SwitchDirection?> GetThrottle(List<CarPosition> pos, int gameTick)
        {
            var res = UpdateThrottle(pos, gameTick);
            var throttle = res.Item1;
#if DEBUG
            if (throttle < 0 || 1.0 < throttle)
            {
                throw new Exception("スロットルの値がおかしい");
            }
#endif
            throttle = Math.Max(throttle, 0.0);
            throttle = Math.Min(throttle, 1.0);
            return Tuple.Create(throttle, res.Item2);
        }

        public void Run()
        {
			//TODO gameTickを全く使っていないので必要なら追加する
            string line;
            send(JoinMessage);
            while ((line = Reader.ReadLine()) != null)
            {
                MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
                Debug.WriteLine(line);
                switch (msg.msgType)
                {
                    case "join":
                        {
                            var data = msg.data.ToObject<Join>();
                        }
                        break;

                    case "yourCar":
                        {
                            MyCarId = msg.data.ToObject<CarId>();
                            Console.WriteLine("MyCar is name:{0} color:{1}", MyCarId.name, MyCarId.color);
                            send(new Ping());
                            UpdatedMyCarId();
                        }
                        break;

                    case "gameInit":
                        {
                            RaceInformation = (msg.data as JObject).GetValue("race").ToObject<Race>();
                            Console.WriteLine("Race init");
                            Console.WriteLine("This race is {0}. {1} cars, {2} laps.", RaceInformation.track.name,
                                RaceInformation.cars.Count, RaceInformation.raceSession.laps);
                            send(new Ping());
                            GameInitialized();
                        }
                        break;

                    case "gameStart":
                        Console.WriteLine("Race starts");
                        send(new Ping());
                        GameStart();
                        break;

                    case "carPositions":
                        {
                            var data = msg.data.ToObject<List<CarPosition>>();
                            var res = GetThrottle(data, msg.gameTick.GetValueOrDefault(0));
                            send(new Throttle(res.Item1));
                            if (res.Item2.HasValue)
                            {
                                send(new SwitchLane(res.Item2.Value));
                            }
                        }
                        break;

                    case "gameEnd":
                        {
                            var data = msg.data.ToObject<GameResult>();
                            Console.WriteLine("Race ended");
                            send(new Ping());
                            GameEnded(data);
                        }
                        break;

                    case "tournamentEnd":
                        Console.WriteLine("Tournament ended");
                        send(new Ping());
                        break;

                    case "crash":
                        //コースの外に出た
                        {
                            var car = msg.data.ToObject<CarId>();
                            Console.WriteLine("Crased : name:{0} color:{1}", MyCarId.name, MyCarId.color);
                            send(new Ping());
                            Crashed(car, msg.gameTick.GetValueOrDefault(0));
                        }
                        break;

                    case "spawn":
                        //コースに復帰した
                        {
                            var car = msg.data.ToObject<CarId>();
                            Console.WriteLine("Spawned : name:{0} color:{1}", MyCarId.name, MyCarId.color);
                            send(new Ping());
                            Spawned(car, msg.gameTick.GetValueOrDefault(0));
                        }
                        break;

                    case "lapFinished":
                        {
                            var lapinfo = msg.data.ToObject<LapInfo>();
                            Console.WriteLine("lap finished");
                            send(new Ping());
                            LapFinished(lapinfo, msg.gameTick.GetValueOrDefault(0));
                        }
                        break;

                    case "dnf":
                        //失格した
                        Console.WriteLine("dnf");
                        send(new Ping());
                        break;

                    case "finish":
                        Console.WriteLine("finished");
                        send(new Ping());
                        break;

                    default:
                        Console.WriteLine(msg.msgType);
                        send(new Ping());
                        break;
                }
            }
        }

        private void send(SendMsg msg)
        {
            Writer.WriteLine(msg.ToJson());
        }
    }
}