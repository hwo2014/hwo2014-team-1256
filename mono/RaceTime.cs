﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace hwo2014
{
	/// <summary>
	/// レース中の時間情報
	/// </summary>
    public class RaceTime
    {
        public int laps { get; set; }
        public int ticks { get; set; }
        public int millis { get; set; }
    }
}
