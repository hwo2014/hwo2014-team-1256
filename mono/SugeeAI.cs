﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace hwo2014
{
    public class SugeeAI : AIBase
    {
        const double SlipEPS = 5;

        private CarPosition.PiecePosition prevPiecePos = null;

		/// <summary>
		/// 円周を求める
		/// </summary>
		/// <param name="radius"></param>
		/// <param name="angle">弧度法による角度</param>
		/// <returns></returns>
        public double getCircumference(double radius, double angle)
        {
            double deg2rad = Math.PI / 180;
            return deg2rad * angle * radius;
        }
        public double getPieceLength(Race.Piece p, CarPosition.Lane lane)
        {
			if(p.length.HasValue)
            {
				return p.length.Value;
            }
			else
            {
				return getCircumference(p.radius.Value
                    - RaceInformation.track.lanes.First(l => l.index == lane.endLaneIndex).distanceFromCenter,
                    p.angle.Value);
            }
        }

        public override Tuple<double,SwitchDirection?> UpdateThrottle(List<CarPosition> pos, int gameTick)
        {
            var mypos = pos.First(p => p.id.color == this.MyCarId.color);

            double velocity = 0.0;
			var curPiecePos = mypos.piecePosition;
			var curPiece = this.RaceInformation.track.pieces[mypos.piecePosition.pieceIndex];

            if (prevPiecePos != null)
            {
                var prevPiece = RaceInformation.track.pieces[prevPiecePos.pieceIndex];
                velocity += getPieceLength(prevPiece, prevPiecePos.lane) - prevPiecePos.inPieceDistance;
                for (int i = prevPiecePos.pieceIndex + 1; i < curPiecePos.pieceIndex; i++)
                {
                    velocity += getPieceLength(RaceInformation.track.pieces[i], curPiecePos.lane);
                }
				velocity += curPiecePos.inPieceDistance;
            }
            prevPiecePos = curPiecePos;
            //Console.WriteLine(velocity);

            var nextPiece = this.RaceInformation.track.pieces[
                (mypos.piecePosition.pieceIndex + 1) % RaceInformation.track.pieces.Count];

            var res = 0.8;
            //if (nextPiece.angle.HasValue)
            //{
            //    res = 0.3;
            //}
            //else
            //{
            //    res = 1.0;
            //}
            if (Math.Abs(mypos.angle) > SlipEPS)
            {
                res = 0.0;
            }
            return Tuple.Create<double, SwitchDirection?>(res, null);
        }
    }
}
