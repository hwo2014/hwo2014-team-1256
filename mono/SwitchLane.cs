﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace hwo2014
{
    class SwitchLane : SendMsg
    {
        public SwitchDirection value;

        public SwitchLane(SwitchDirection value)
        {
            this.value = value;
        }

        protected override Object MsgData()
        {
            return this.value.ToString();
        }

        protected override string MsgType()
        {
            return "switchLane";
        }
    }
}
