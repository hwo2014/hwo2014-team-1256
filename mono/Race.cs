﻿//Generated at http://json2csharp.com/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace hwo2014
{
    public class Race
    {
		/// <summary>
		/// コースの各部分情報(直線と曲線でクラスを分けれたら分けたいかも
		/// </summary>
        public class Piece
        {
			/// <summary>
			/// 直線の場合の長さ
			/// </summary>
            public double? length { get; set; }
			/// <summary>
			/// レーンを切り替えることのできる位置かどうか
			/// </summary>
            public bool? IsSwitch { get; set; }
			/// <summary>
			/// 半径
			/// </summary>
            public int? radius { get; set; }
			/// <summary>
			/// 左回りはマイナス、右回りはプラス
			/// </summary>
            public double? angle { get; set; }
        }

		/// <summary>
		/// 各レーンの情報
		/// </summary>
        public class LaneInfo
        {
            public int distanceFromCenter { get; set; }
            public int index { get; set; }
        }

		/// <summary>
		/// 描画用の始点情報
		/// </summary>
        public class StartingPoint
        {
            public Point2D position { get; set; }
            public double angle { get; set; }
        }

        public class Track
        {
            public string id { get; set; }
            public string name { get; set; }
			/// <summary>
			/// 各走行部分の情報
			/// </summary>
            public List<Piece> pieces { get; set; }
			/// <summary>
			/// 各レーンの情報(走るところの中心からの距離がとれる)
			/// </summary>
            public List<LaneInfo> lanes { get; set; }
			/// <summary>
			/// 描画用の始点情報
			/// </summary>
            public StartingPoint startingPoint { get; set; }
        }

		/// <summary>
		/// 車体の物理的情報
		/// </summary>
        public class Dimensions
        {
            public double length { get; set; }
            public double width { get; set; }
            public double guideFlagPosition { get; set; }
        }

        public class Car
        {
            public CarId id { get; set; }
            public Dimensions dimensions { get; set; }
        }

        public class RaceSession
        {
            public int laps { get; set; }
            public int maxLapTimeMs { get; set; }
            public bool quickRace { get; set; }
        }

		/// <summary>
		/// このゲームで走るトラックの情報
		/// </summary>
        public Track track { get; set; }
        public List<Car> cars { get; set; }
        public RaceSession raceSession { get; set; }
    }
}
