﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace hwo2014
{
	/// <summary>
	/// スイッチする向き
	/// </summary>
    public enum SwitchDirection
    {
		Left, Right
    }
}
