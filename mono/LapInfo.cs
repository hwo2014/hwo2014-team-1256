﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace hwo2014
{
    public class LapInfo
    {
        public class Ranking
        {
            public int overall { get; set; }
            public int fastestLap { get; set; }
        }

        public CarId car { get; set; }
        public RaceTime lapTime { get; set; }
        public RaceTime raceTime { get; set; }
        public Ranking ranking { get; set; }
    }
}
