﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace hwo2014
{
    public class GameResult
    {
        public class Result
        {
            public RaceTime result { get; set; }
            public CarId car { get; set; }
        }

        public List<Result> results{get;set;}
        public List<Result> beatLaps { get; set; }
    }
}
