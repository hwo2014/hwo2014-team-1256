﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace hwo2014
{
    public class CarPosition
    {
		/// <summary>
		/// このピースに入ったときのレーンの情報
        /// 普通はstartとendは同じだが、レーンをスイッチしたときには違います
		/// </summary>
        public class Lane
        {
            public int startLaneIndex { get; set; }
            public int endLaneIndex { get; set; }

            public Lane Clone()
            {
                var res = new Lane();
                res.startLaneIndex = this.startLaneIndex;
                res.endLaneIndex = this.endLaneIndex;
                return res;
            }
        }

        public class PiecePosition
        {
			/// <summary>
			/// 0から始まるピースの位置番号
			/// </summary>
            public int pieceIndex { get; set; }
			/// <summary>
			/// ピース中の開始点からのガイドフラッグの位置
			/// </summary>
            public double inPieceDistance { get; set; }
			/// <summary>
			/// このピースに入るときと出るときのレーンの情報
			/// </summary>
            public Lane lane { get; set; }
			/// <summary>
			/// 完了したラップ数(0:最初のラップ中、-1:最初のラップでスタート地点をまたいですらいない)
			/// </summary>
            public int lap { get; set; }

            public PiecePosition Clone()
            {
                var res = new PiecePosition();
                res.pieceIndex = this.pieceIndex;
                inPieceDistance = this.inPieceDistance;
                lane = this.lane.Clone();
                lap = this.lap;
                return res;
            }
        }

        public CarId id { get; set; }
		/// <summary>
        /// 車のスリップしてる角度(slip angle)、スピード出し過ぎとか、クラッシュする限界値があるらしいです。
        /// 普通は0
		/// </summary>
        public double angle { get; set; }
        public PiecePosition piecePosition { get; set; }
    }
}
